using Mirror;
using UnityEngine;
using UnityEngine.AI;

public class UnitMovement : NetworkBehaviour
{
    [SerializeField] private NavMeshAgent navMeshAgent = null;
    [SerializeField] private Targeter targeter = null;
    [SerializeField] private float chaseRange = 10f;

    #region Server

    [ServerCallback]
    private void Update()
    {
        Targetable target = targeter.GetTarget();

        if (target != null)
        {   // avoids doing the sqrt of a positino by comparing the squared length of the vector against chaseRange squared
            if ((target.transform.position - transform.position).sqrMagnitude > chaseRange * chaseRange)
            {
                // chase
                navMeshAgent.SetDestination(target.transform.position);
            }
            else if (navMeshAgent.hasPath)
            {
                // stop chasing
                navMeshAgent.ResetPath();
            }

            return;
        }

        // sometimes the navmesh agent takes more than a frame to calculate a route,
        // so we check that it isn't trying to go anywhere before proceeding
        if (!navMeshAgent.hasPath) { return; }

        // is the unit close to its destination? 
        if (navMeshAgent.remainingDistance > navMeshAgent.stoppingDistance) { return; }

        // unit is within stopping distance, so clear path
        navMeshAgent.ResetPath();
    }

    [Command]
    public void CmdMove(Vector3 position)
    {
        targeter.ClearTarget();

        // is this a valid destination, or within 0.5f of a valid destination?
        if (!NavMesh.SamplePosition(position, out NavMeshHit hit, 0.5f, NavMesh.AllAreas)) { return; }

        // position was valid, set as destination. use hit.point in case the sampled
        // position was corrected slightly
        navMeshAgent.SetDestination(hit.position);
    }

    #endregion
}
