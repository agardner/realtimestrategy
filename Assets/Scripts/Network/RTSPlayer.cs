using System;
using System.Collections;
using System.Collections.Generic;
using Mirror;
using UnityEngine;

public class RTSPlayer : NetworkBehaviour
{
    [SerializeField] private List<Unit> myUnits = new List<Unit>();

    public List<Unit> GetUnits()
    {
        return myUnits;
    }

    #region Server

    public override void OnStartServer()
    {
        Unit.ServerOnUnitSpawned += ServerHandleUnitSpawned;
        Unit.ServerOnUnitDespawned += ServerHandleUnitDespawned;
    }

    private void ServerHandleUnitSpawned(Unit obj)
    {
        // if this isn't a unit belonging to the current player, don't do anything, otherwise all players would know about every unit
        if (obj.connectionToClient.connectionId != connectionToClient.connectionId) { return; }

        myUnits.Add(obj);
    }

    public override void OnStopServer()
    {
        Unit.ServerOnUnitSpawned -= ServerHandleUnitSpawned;
        Unit.ServerOnUnitDespawned -= ServerHandleUnitDespawned;
    }

    private void ServerHandleUnitDespawned(Unit obj)
    {
        if (obj.connectionToClient.connectionId != connectionToClient.connectionId) { return; }

        myUnits.Remove(obj);
    }

    #endregion

    #region Client

    public override void OnStartClient()
    {
        if (!isClientOnly) { return; }

        Unit.AuthorityOnUnitSpawned += AuthorityHandleUnitSpawned;
        Unit.AuthorityOnUnitDespawned += AuthorityHandleUnitDespawned;
    }

    private void AuthorityHandleUnitDespawned(Unit obj)
    {
        if (!hasAuthority) { return; }

        myUnits.Remove(obj);
    }

    private void AuthorityHandleUnitSpawned(Unit obj)
    {
        if (!hasAuthority) { return; }

        myUnits.Add(obj);
    }

    public override void OnStopClient()
    {
        if (!isClientOnly) { return; }

        Unit.AuthorityOnUnitSpawned -= AuthorityHandleUnitSpawned;
        Unit.AuthorityOnUnitDespawned -= AuthorityHandleUnitDespawned;
    }

    #endregion
}
